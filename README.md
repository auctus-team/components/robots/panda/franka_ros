### About this fork

This fork is a mirror of [franka_ros](https://github.com/frankaemika/franka_ros).

The purpose is to maintain an AUCTUS version of with updated urdf models (for instance, intertial parameters and other changes that may be added and can be checked through the commit history).

#### Quickstart for developers (users)

The main stable branch with auctus changes is called `auctus`.
Please do not submit changes to this branch, create a new one with a corresponding merge request for review.

To start using the library:

```
git clone git@gitlab.inria.fr:auctus-team/components/robots/panda/franka_ros.git
git checkout auctus
```

#### For fork maintainers

This fork must be kept up to date manually.

As of this moment, the only branch of interest is `develop`, which is the one used as an example in the `how to update` section.

**Setting up the fork for the first time**

This sets a second remote as `upstream` (remotes can be checked with `git remote -v`), allowing to have `origin` pointed to this repository (the fork) and `upstream` to the original github repository (that we'll use to update the main branches).

```
git clone git@gitlab.inria.fr:auctus-team/components/robots/panda/franka_ros.git
git remote add upstream https://github.com/frankaemika/franka_ros.git
```

**Updating a branch**

```
git checkout develop
git pull upstream
git push -u origin develop
```

You can also use `git push -u origin --all` to push all local branches (normally, after following the steps above, you will only have a local `develop` that synced with `remotes/upstream/develop`). All local and remote branches can be seen with `git branch -a`.

---

# ROS integration for Franka Emika research robots

[![CI](https://github.com/frankaemika/franka_ros/actions/workflows/ci.yml/badge.svg)](https://github.com/frankaemika/franka_ros/actions/workflows/ci.yml)


See the [Franka Control Interface (FCI) documentation][fci-docs] for more information.

## License

All packages of `franka_ros` are licensed under the [Apache 2.0 license][apache-2.0].

[apache-2.0]: https://www.apache.org/licenses/LICENSE-2.0.html
[fci-docs]: https://frankaemika.github.io/docs
